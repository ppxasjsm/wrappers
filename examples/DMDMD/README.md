The user might consider evaluating whether the following installation/"module load"-s have been performed in his/her workstation:

LsdMap installation:

```
cd $HOME
git clone https://github.com/CharlieLaughton/lsdmap.git
cd lsdmap/
python setup.py install --user
```
and:
```
module load amber
module load gromacs
```



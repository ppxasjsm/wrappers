from wrappers import  amber, pycoco
import os

# set number of cycles, and number of replicates
maxcycles = 10
nreps = 8

# create a list that will hold all the trajectory file names
mdtrajectories = []

# put intermediate files in a subdirectory to keep things tidy:
defdir = 'cocofiles'

# define some defaults for the MD and CoCo analysis

amber.DEFAULTS['-p'] = 'penta.top'
amber.DEFAULTS['-c'] = 'penta.crd'
amber.DEFAULTS['-x'] = '{}.nc'

pycoco.DEFAULTS['-o'] = '{}.rst7'
pycoco.DEFAULTS['-f'] = 'rst'
pycoco.DEFAULTS['-t'] = 'penta.pdb'
pycoco.DEFAULTS['-n'] = nreps
pycoco.DEFAULTS['--dims'] = 3
pycoco.DEFAULTS['--grid'] = 30

#
# Main loop begins here:
#
for cycle in range(maxcycles):
    if cycle == 0:
#
# Cycle 0: We begin with a single coordinate and topology file. 
# Create a "dummy" coco results dictionary with copies of the initial
# coordinates under the '-o' key:
#
        print 'setting up...'
        cocores = {}
        cocores['-o'] = ['penta.crd',] * nreps
    else:        
        print 'Cycle {}: running CoCo...'.format(cycle-1)
        
        # Set pyCoCo parameters:
        coinp = pycoco.new_inputs(defdir=defdir)
        coinp['-i'] = mdtrajectories

        # Run job:
        cocores = pycoco.run(coinp)

#
# Now run the MD.
#
    for rep in range(nreps):
        # set parameters:
        mininp = amber.new_inputs(defdir=defdir)
        mininp['-ref'] = cocores['-o'][rep]
        mininp['-i'] = 'min.in'

        # run job:
        print 'Cycle {} replicate {}: running energy minimisation step...'.format(cycle, rep)
        minres = amber.run(mininp)

        # set md run parameters:
        mdinp = amber.new_inputs(defdir=defdir)
        mdinp['-c'] = minres['-r']
        mdinp['-i'] = 'md.in'
        mdinp['-x'] = '{}/cycle{}rep{}.nc'.format(defdir, cycle, rep)

        # run job:
        print 'Cycle {} replicate {}: running md step...'.format(cycle, rep)
        mdres = amber.run(mdinp)

        # stash results
        mdtrajectories.append(mdres['-x'])

#
# final CoCo run...
#
print 'Cycle {}: running CoCo...'.format(cycle)
        
coinp = pycoco.new_inputs(defdir=defdir)
coinp['-i'] = mdtrajectories

cocores = pycoco.run(coinp)

print 'final results in CoCo_final.log'
os.rename(cocores['-l'], 'CoCo_final.log')
os.system('rm '+defdir+'/tmp*')
